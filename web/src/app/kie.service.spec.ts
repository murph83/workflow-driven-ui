import { TestBed, inject } from '@angular/core/testing';

import { KieService } from './kie.service';

describe('KieService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KieService]
    });
  });

  it('should be created', inject([KieService], (service: KieService) => {
    expect(service).toBeTruthy();
  }));
});
