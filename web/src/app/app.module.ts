import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MastheadComponent} from './masthead/masthead.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {UserService} from './user.service';
import {DefaultPipe} from './default.pipe';
import {WelcomeComponent} from './welcome/welcome.component';
import {EmptyStateModule} from 'patternfly-ng';
import {HttpClientModule} from '@angular/common/http';
import {KieConfig, KieService} from "./kie.service";
import { CustomerComponent } from './customer/customer.component';
import {FormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";
import {TaskService} from "./task.service";

const kieConfig = {
  url: '/kie-server/services/rest/server',
  username: 'bpmsAdmin',
  password: 'jbossadmin1!',
  containerId: 'rh.demo:ht-onboarding:0.1.2'
} as KieConfig;

const appRoutes: Routes = [
  {path: 'customer', component: CustomerComponent},
  {path: '**', component: WelcomeComponent}
]



@NgModule({
  declarations: [
    AppComponent,
    MastheadComponent,
    DefaultPipe,
    WelcomeComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    EmptyStateModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService,
              {provide: 'KieConfig', useValue: kieConfig},
              KieService,
              TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
