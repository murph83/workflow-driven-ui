import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {Task, TaskService} from "./task.service";

export interface KieConfig {
  url: string;
  username: string;
  password: string;
  containerId: string;
}

const httpHeaders: HttpHeaders = new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json');

@Injectable()
export class KieService {

  constructor(private http: HttpClient, @Inject('KieConfig') private kieConfig: KieConfig, private router: Router, private taskService: TaskService) {
  }


  startEnrollment() {
    this.http.post(this.kieConfig.url + '/containers/' + this.kieConfig.containerId + '/processes/' + 'ht-onboarding.onboarding' + '/instances',
      {}, {headers: httpHeaders}).subscribe(data => {
      this.getNextTask(data.toString())
    });

  }

  getNextTask(p: string) {
    this.http.get(this.kieConfig.url + '/queries/tasks/instances/process/' + p, {headers: httpHeaders}).subscribe(data => {
      const t = {id: data['task-summary'][0]['task-id'], name: 'customer'} as Task;
      this.taskService.setCurrentTask(t);
      this.router.navigate([t.name]);
    });
  }

  completeTask(m: any) {
    this.http.put(this.kieConfig.url + '/containers/'+ this.kieConfig.containerId +'/tasks/' + this.taskService.getCurrentTask() + 'started',{},{headers:httpHeaders}).subscribe();
    this.http.put(this.kieConfig.url + '/containers/'+ this.kieConfig.containerId +'/tasks/' + this.taskService.getCurrentTask() + 'completed',{'customer': m},{headers:httpHeaders}).subscribe();
  }


}
