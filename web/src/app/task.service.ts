import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

export interface Task {
  id: string;
  name: string;
}

@Injectable()
export class TaskService {

  private _task: BehaviorSubject<Task> = new BehaviorSubject<Task>({} as Task);
  public task: Observable<Task> = this._task.asObservable();

  constructor() { }

  setCurrentTask(task: Task){
    this._task.next(task);
  }

  getCurrentTask(): Task {
    return this._task.getValue();
  }

}
