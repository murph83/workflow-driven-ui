import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";

export interface User {
  name: string;
  email: string;
  username: string;
  verified: boolean;
}

@Injectable()
export class UserService {

  private _user: BehaviorSubject<User> = new BehaviorSubject({} as User);
  public readonly user: Observable<User> = this._user.asObservable();

  constructor() {}

  update(user: User){
    const u = {...this._user.getValue(), ...user} as User;
    this._user.next(u);
  }

}
