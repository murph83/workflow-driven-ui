import { Component, OnInit } from '@angular/core';
import {Action, ActionConfig, EmptyStateConfig} from "patternfly-ng";
import {User, UserService} from "../user.service";
import {KieService} from "../kie.service";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  actionConfig: ActionConfig;
  emptyStateConfig: EmptyStateConfig;

  constructor(private userService: UserService, private kieService: KieService) { }

  ngOnInit() {
    this.actionConfig = {
      primaryActions: [{
        id: 'enroll',
        title: 'Enroll',
        tooltip: 'Enroll a new user'
      }],
      moreActions: []
    } as ActionConfig;

    this.emptyStateConfig = {
      actions: this.actionConfig,
      title: 'Welcome'
    } as EmptyStateConfig;
  }

  handleAction($event: Action){
    this.kieService.startEnrollment();
  }

}
