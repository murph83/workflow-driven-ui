import { Component, OnInit } from '@angular/core';
import {Customer} from "./customer";
import {KieService} from "../kie.service";
import {TaskService} from "../task.service";

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  model: Customer;

  constructor(private kieService: KieService) { }

  ngOnInit() {
    this.model = new Customer();
  }

  onSubmit(){
    this.kieService.completeTask(this.model);
  }

}
