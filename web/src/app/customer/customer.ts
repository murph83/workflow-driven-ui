export class Customer {

  constructor(
    public name?: string,
    public username?: string,
    public email?: string,
    public address?: string,
    public dob?: Date
  ){}

}
