import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'default',
})
export class DefaultPipe implements PipeTransform {

  transform(value: any, arg: string): any {
    return !value && arg ? arg : value;
  }

}
