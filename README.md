Workflow Driven UI
=======================

The projects in this repository fall into two categories

Front End
---------
* `web`
This is the Angular4/Patternfly application for rendering the onboarding process


Back End
--------
* `onboarding-domain`
* `ht-onboarding`
These projects are used by the BPM runtime. `onboarding-domain` is the domain model for the project, `ht-onboarding` is the human-task modeled onboarding process.


ToDo
----
* Embedded engine example (for now, see [Maciej's example](https://github.com/mswiderski/spring-jbpm-app))
* Signal modeled onboarding process
